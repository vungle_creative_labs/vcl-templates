# Vungle Templates #

This README takes you through the process of creating an ad which is compatible on the Vungle network.



## Requirements ##
When creating a Vungle-compatible ad experience, the following criteria MUST be followed or it may fail QA:

### ✅ Do ###
1. Do name your main html file `index.html`.
2. Do ensure you use the [Complete](#complete) event to notify the SDK when the ad experience is complete. This should be after the user has completed the interactive element(s) of the creative. Triggering this when the end screen with CTA loads would be a good example.
3. Do check that there are no javascript errors in your ad, and ensure it passes markup validation (use a validator such as W3C, or lint when developing locally).
4. Ensure your creative is compatible across both orientations, and if necessary use aspect ratio breakpoints.
5. Ensure a CTA is actionable and visible during the experience.
6. Do ensure you use the [Ad Pause](#ad-pause)/[Ad Resume](#ad-resume) events to control video and audio if the ad is paused or resumed by the SDK.
7. Ensure all assets are compressed, and the total file size of the ad should not exceed 5MB.

### 🚫 Do NOT ###
1. Do not use a nested directory structure. All files (e.g CSS, JS and other assets) must be kept at the same level as the `index.html` or `ad.html`.
2. Do not use `vw` and `vh` in the CSS.
3. Do not use `document.location.reload`
4. Do not use your own close button. Our template will add a close button automatically.
5. Do not use external links for assets. All assets must be referenced locally within the creative.
6. Do not use ad tracking links or any external tracking libraries.
7. Do not load assets externally. Due to CORS (cross-origin), files need to be loaded within the creative. Use base64 if necessary.
8. Do not include references to MRAID files, such as `mraid.js`.
9. Do not automatically fire click events. Our template will handle additional click events automatically.
10. Do not use code that would be invalid or fail markup validation (please use a markup validator such as [W3C](https://validator.w3.org) to check your code is complete and working correctly). This ensures it will work consistently across all browsers that are supported in our different SDK platforms (iOS, Android, Windows and Amazon).



## Making a Creative ##
#### CTA
The CTA should call `parent.postMessage('download','*')` and should only be used when a user actions/clicks on the CTA. For example:

```html
<a href="#" onclick="parent.postMessage('download','*')">Download</a>
```

#### Complete
When the playable/interactive section of an ad experience finishes, the playable should call `parent.postMessage('complete','*')`.
For example, when the game experience is complete and the only remaining action on the playable experience would be a CTA click (i.e. an end screen). The complete event should not be tied to a CTA/Download click or be triggered without any interaction.

```javascript
function onGameComplete() {
	parent.postMessage('complete','*');
}
```

The `complete` event should not be tied to a CTA/Download click or trigger without any interaction or on the first interaction.

#### Ad Pause
Use this event to pause audio/video/animations. This event occurs when the ad is temporarily not visible to the user; for example, if an alert appears above the ad, or if the user exits the app whilst watching the ad.
```javascript
window.addEventListener('ad-event-pause', function() {
	// Pause audio/video/animations inside here
});
```

#### Ad Resume
Use this event to resume audio/video/animations. This event occurs when the ad is once again visible to the user; for example, after dismissing an alert which appeared above the ad, or when the user resumes the app when the ad was playing.
```javascript
window.addEventListener('ad-event-resume', function() {
	// Resume audio/video/animations inside here
});
```



## Developing Ads for Windows ##
Any playable that is intended to run on Windows needs to account for additional aspect ratios and resolutions, such as one of the following:

* Desktop full-screen
* Desktop window (similar to an aspect ratio/resolution of a mobile device)
* Other extreme aspect ratios (for example):
 * 2:5
 * 5:2 long/tall
 * 1:1 square windows