const gulp = require('gulp');
const babel = require('gulp-babel');
const uglify = require('gulp-uglify');
const concat = require('gulp-concat');
const rename = require('gulp-rename');
const download = require('gulp-download');

const inputDir = 'src';
const outputDir = 'dist';

// build task 

gulp.task('build', ['html', 'js', 'fetch-vungle-wrapper']);

// rename index.html to ad.html

gulp.task('html', function() {

    return gulp.src(`${inputDir}/index.html`)
        .pipe(rename('ad.html'))
        .pipe(gulp.dest(`${outputDir}`));
});

// download the vungle wrapper from S3 bucket

gulp.task('fetch-vungle-wrapper', function() {

    const vungleIndexURL = 'https://s3.eu-west-2.amazonaws.com/vungle-templates/index.html';

    return download(vungleIndexURL)
        .pipe(gulp.dest(`${outputDir}/`));
});

// include es polyfill and transpile es6+ code into es2015

gulp.task('js', function() {

    return gulp.src([
            'node_modules/@babel/polyfill/dist/polyfill.js',
            `${inputDir}/playable.js`
        ])
        .pipe(babel({
            presets: ['@babel/preset-env']
        }))
        .pipe(concat('playable.js'))
        .pipe(uglify()) // minify
        .pipe(gulp.dest(outputDir))
});