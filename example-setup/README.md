# EXAMPLE SETUP #

Here is a stripped down example project setup which can generate vungle compliant templates using gulp

### BEFORE YOU START ###
Install gulp (see https://gulpjs.org/getting-started.html)

### GETTING STARTED ###

Install the node modules and run gulp build

```
npm i
gulp build
```